#!/bin/bash

here=`pwd`
find ~ -name \.git -print 2>/dev/null | sed 's#.git##' | grep -v "/home/annegf/.atom" | while read dir
do
    echo "Pull on "${dir}
    cd $dir
    git pull
done
cd $here
