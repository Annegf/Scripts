# Trucs & Astuces sous Linux/Ubuntu

## Activer la touche d'activation/désactivation du wiki d'un claivier d'un portable EliteBook HP
`sudo setkeycodes e078 238`

Src : http://www.linlap.com/hp_elitebook_8460p

## Choisir les programmes lancés au démarrage
Installer gnome-tweak :
`sudo aptitude install gnome-tweak-tool`
et lancer :
`gnome-tweaks`

## Désactiver le bluetooth au déamrrage
`sudo gedit /etc/bluetooth/main.conf`
et tout en bas, changer la ligne : `AutoEnable=true` en `AutoEnable=false`

