#!/bin/bash

here=`pwd`
for dir in `find ~ -name \.git -print 2>/dev/null | sed 's#.git##'`
do
    echo -e "\nStatus on \e[4m"${dir}"\e[24m ? (*/n)\c"
    read answer
    if [[ $answer = "n" ]]
    then
	continue
    else
	cd $dir
	git status
    fi
done
cd $here
